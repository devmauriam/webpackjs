const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
module.exports = {
    output:{
        clean:true,
    },
    mode:'development',
    module: {
        rules:[
            {
                test:/\.html$/i,
                loader: 'html-loader',
                options :{
                    source:false
                }
            },
            {
                test:/\.css$/i,
                exclude:/main.css$/i,
                use:["style-loader","css-loader"]
            },
            {
                test:/main.css$/i,
                use:[MiniCssExtractPlugin.loader,'css-loader']
            },
            {
                test:/\.png/,
                type: 'asset/resource',
                generator:{
                    filename: 'static/[hash][ext][query]'
                }
            }
        ]
    },
    plugins:[
        new HtmlWebpackPlugin({
            templates: 'src/index.html',
            title: ' My WebPack APP'
        }),
        new MiniCssExtractPlugin({
            filename:'main.css'
        })
    ]
}